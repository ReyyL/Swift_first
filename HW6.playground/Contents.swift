import UIKit


//Задание 1
print("Задание 1")
print()

struct Candidate {
    enum Grade {
        case junior
        case middle
        case senior
    }
    
    let grade: Grade
    let requiredSalary: Int
    let fullName: String
}

protocol Filter {
    func filterCandidates(_ candidates: [Candidate]) -> [Candidate]
}

struct GradeFilter: Filter {
    let grade: Candidate.Grade
    
    func filterCandidates(_ candidates: [Candidate]) -> [Candidate] {
        return candidates.filter { $0.grade == grade }
    }
}

struct SalaryFilter: Filter {
    let salary: Int

    func filterCandidates(_ candidates: [Candidate]) -> [Candidate] {
        return candidates.filter { $0.requiredSalary <= salary }
    }
}

struct NameFilter: Filter {
    let name: String

    func filterCandidates(_ candidates: [Candidate]) -> [Candidate] {
        return candidates.filter { $0.fullName.contains(name) }
    }
}


var candidates = [Candidate(grade: .junior, requiredSalary: 100000, fullName: "Ванек"),
                  Candidate(grade: .middle, requiredSalary: 200000, fullName: "Санек"),
                  Candidate(grade: .senior, requiredSalary: 300000, fullName: "Иванесса"),
                  Candidate(grade: .middle, requiredSalary: 250000, fullName: "Макс"),
                  Candidate(grade: .junior, requiredSalary: 150000, fullName: "Анастасия"),
                  Candidate(grade: .senior, requiredSalary: 400000, fullName: "Никита")]

let gradeFilter = GradeFilter(grade: .middle)
let filteredByGrade = gradeFilter.filterCandidates(candidates)
printCandidates(filteredByGrade)
print()

let salaryFilter = SalaryFilter(salary: 250000)
let filteredBySalary = salaryFilter.filterCandidates(candidates)
printCandidates(filteredBySalary)
print()

let nameFilter = NameFilter(name: "ан")
let filteredByName = nameFilter.filterCandidates(candidates)
printCandidates(filteredByName)
print()

func printCandidates(_ candidates: [Candidate]) {
    for candidate in candidates {
        print("Кандидат: \(candidate.fullName), requiredSalary: \(candidate.requiredSalary), grade: \(candidate.grade)")
    }
}


//Задание 2
print("Задание 2")
print()

extension Candidate.Grade {
    var gradesOrder: Int {
        switch self {
        case .junior:
            return 1
        case .middle:
            return 2
        case .senior:
            return 3
        }
    }
}

struct BiggerGradeFilter: Filter {
    let biggerGrade: Candidate.Grade

    func filterCandidates(_ candidates: [Candidate]) -> [Candidate] {
        return candidates.filter { $0.grade.gradesOrder >= biggerGrade.gradesOrder }
    }
}

let biggerGradeFilter = BiggerGradeFilter(biggerGrade: .middle)
let filteredByBiggerGrade = biggerGradeFilter.filterCandidates(candidates)
printCandidates(filteredByBiggerGrade)
