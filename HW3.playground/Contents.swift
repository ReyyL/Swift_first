import UIKit

var buffer = makeBuffer()

func makeBuffer() -> (String) -> Void {
    var sentence = ""
    
    func addWords(arg: String) {
        sentence += arg
        if arg.isEmpty {
            print(sentence)
        }
    }
    
    return addWords
}

buffer("Замыкание")
buffer(" использовать")
buffer(" нужно!")
buffer("")
