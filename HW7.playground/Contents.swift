import UIKit

class NameList {
    var names: [String:[String]] = [:]
    
    func addName(name: String) -> () {
        var firstLetter = String(name.prefix(1))
        
        if names[firstLetter] == nil {
            names[firstLetter] = []
        }
        
        names[firstLetter]?.append(name)
    }
    
    func printNames() -> () {
        let sortedKeys = names.keys.sorted()
        
        for key in sortedKeys {
            print(key)
            for name in names[key]! {
                if name == "" {
                    continue
                }
                else {
                    print(name)
                }
            }
        }
    }
}

let list = NameList()
list.addName(name: "Акулова")
list.addName(name: "Валерьянов")
list.addName(name: "Бочкин")
list.addName(name: "Бабочкина")
list.addName(name: "Арбузов")
list.addName(name: "Васильева")
list.addName(name: "Яшин")
list.addName(name: "")
list.printNames()
