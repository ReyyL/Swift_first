import UIKit


//1 задание
print("1 задание")
print()

class Shape {
    func calculateArea() -> Double {
        fatalError("not implemented")
    }
    func calculatePerimeter() -> Double {
        fatalError("not implemented")
    }
}

class Rectangle: Shape {
    private let height: Double
    private let width: Double
    
    init(height: Double, width: Double) {
        self.height = height
        self.width = width
    }
    
    override func calculateArea() -> Double {
        height * width
    }
    
    override func calculatePerimeter() -> Double {
        2 * (height + width)
    }
}

class Circle: Shape {
    private let radius: Double
    
    init(radius: Double) {
        self.radius = radius
    }
    
    override func calculateArea() -> Double {
        Double.pi * pow(radius, 2)
    }
    
    override func calculatePerimeter() -> Double {
        2 * Double.pi * radius
    }
}

class Square: Shape {
    private let side: Double
    
    init(side: Double) {
        self.side = side
    }
    
    override func calculateArea() -> Double {
        pow(side, 2)
    }
    
    override func calculatePerimeter() -> Double {
        4 * side
    }
}

var shapes: [Shape] = []

let rectangle = Rectangle(height: 11.0, width: 5.0)
let circle = Circle(radius: 7.0)
let square = Square(side: 2.0)

shapes.append(rectangle)
shapes.append(circle)
shapes.append(square)

var areaSum: Double = 0
var perimeterSum: Double = 0

for shape in shapes {
    areaSum += shape.calculateArea()
    perimeterSum += shape.calculatePerimeter()
}

print("Общая площадь фигур:", areaSum)
print("Общий периметр фигур:", perimeterSum)
print()


//2 задание

print("2 задание")
print()

func findIndexWithGenerics<T: Equatable>(of valueToFind: T, in array: [T]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
let arrayOfIntegers = [4, 8, 11, 5, 2]
let arrayOfDoubles = [1.8, 32.4, 11.0, 87.1]

if let foundIndexString = findIndexWithGenerics(of: "лама", in: arrayOfString) {
    print("Индекс искомой строки: \(foundIndexString)")
}

if let foundIndexDouble = findIndexWithGenerics(of: 1.8, in: arrayOfDoubles) {
    print("Индекс искомого дробного числа: \(foundIndexDouble)")
}

if let foundIndexInt = findIndexWithGenerics(of: 5, in: arrayOfIntegers) {
    print("Индекс искомого целого числа: \(foundIndexInt)")
}




