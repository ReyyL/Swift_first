import UIKit

func checkPrimeNumber(_ number: Int) -> Bool {
    guard number >= 2 else { return false }
    for i in 2..<number {
        if number % i == 0 { return false }
    }
    return true
}

checkPrimeNumber(1)
checkPrimeNumber(2)
checkPrimeNumber(13)
