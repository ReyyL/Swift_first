import Foundation

//1 задание
var milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

//2 задание
var milkPrice: Double = 3

//3 задание
milkPrice = 4.20

//4 задание
var milkBottleCount: Int? = 20
var profit: Double = 0.0

if let unwrappedMilkBottles = milkBottleCount {
    profit = milkPrice * Double(unwrappedMilkBottles)
}
print(profit)

//принудительное развертывание (milkBottleCount!) не используется тк может привести к крашу, если значение переменной будет nil
//другой пример
var name: String?

if let yourName = name {
    print("Тебя зовут ", yourName)
} else {
    print("Твое имя неизвестно")
}

//5 задание
var employeesList = [String]()
employeesList + ["Иван", "Олег", "Кирилл", "Илья", "Антон"]

//6 задание
var isEveryoneWorkHard: Bool = false
var workingHours = 30

isEveryoneWorkHard = workingHours >= 40
print(isEveryoneWorkHard)
